﻿using System;

namespace backend.domain.Requests
{
    public class CreateReservationRequest
    {
        public DateTime Date { get; set; }
        public int CustomerId { get; set; }

        private CreateReservationRequest(DateTime date, int customerId)
        {
            Date = date.ToUniversalTime();
            CustomerId = customerId;
        }

        public static CreateReservationRequest Create(DateTime date, int customerId)
        {
            return new CreateReservationRequest(date, customerId);
        }
    }
}
