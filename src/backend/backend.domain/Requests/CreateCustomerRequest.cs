﻿using System;

namespace backend.domain.Requests
{
    public class CreateCustomerRequest
    {
        public string Name { get; set; }
        public string Surname { get; set; }

        private CreateCustomerRequest(string name, string surname)
        {
            Name = name;
            Surname = surname;
        }

        public static CreateCustomerRequest Create(string name, string surname)
        {
            return new CreateCustomerRequest(name, surname);
        }
    }
}
