﻿using System;

namespace backend.domain
{
    public class Reservation
    {
        public DateTime Date { get; set; }

        public int CustomerId { get; set; }

        private Reservation(DateTime date, int customerId)
        {
            Date = date;
            CustomerId = customerId;
        }

        public static Reservation Create(DateTime date, int customerId)
        {
            return new Reservation(date, customerId);
        }
    }
}