﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using backend.application.Services;
using backend.domain;
using backend.domain.Requests;
using backend.infrastructure.Repositories.Interfaces;

using Microsoft.Extensions.Logging;

using Moq;

using Xunit;

namespace backend.application.tests.Services
{
    [ExcludeFromCodeCoverage]
    public class ReservationServiceTests
    {
        private readonly Mock<ILogger<ReservationService>> _loggerMock;
        private readonly Mock<IBookingDbRepository> _bookingDbRepoMock;

        public ReservationServiceTests()
        {
            _loggerMock = new Mock<ILogger<ReservationService>>();
            _bookingDbRepoMock = new Mock<IBookingDbRepository>();
        }

        [Fact]
        public async Task CreateReservationAsyncTest()
        {
            // Arrange
            var request = DefaultReservationRequest();
            var sut = new ReservationService(_loggerMock.Object, _bookingDbRepoMock.Object);

            // Act
            await sut.CreateReservationAsync(request);

            // Assert
            _bookingDbRepoMock.Verify(mock => mock.CreateReservationAsync(
                It.Is<CreateReservationRequest>(req => req.Date.Equals(request.Date) && req.CustomerId == request.CustomerId)),
                Times.Once);
        }

        [Fact]
        public async Task GivenExceptionIsThrownByDBRepo_ThenCreateReservationAsyncRethrows()
        {
            // Arrange
            var request = DefaultReservationRequest();

            _bookingDbRepoMock
                .Setup(mock => mock.CreateReservationAsync(It.IsAny<CreateReservationRequest>()))
                .ThrowsAsync(new IOException());

            var sut = new ReservationService(_loggerMock.Object, _bookingDbRepoMock.Object);

            // Act & Assert
            var exception = await Assert.ThrowsAsync<IOException>(() => sut.CreateReservationAsync(request));
        }

        [Fact]
        public async Task GetReservationsByCustomerIdTest()
        {
            // Arrange
            var customerId = 1;
            var date = DateTime.Today;
            var domainReservation = Reservation.Create(date, customerId);


            _bookingDbRepoMock
                .Setup(mock => mock.GetReservationsByCustomerIdAsync(customerId))
                .ReturnsAsync(new List<Reservation>() { domainReservation });

            var sut = new ReservationService(_loggerMock.Object, _bookingDbRepoMock.Object);

            // Act
            var result = await sut.GetReservationsByCustomerId(customerId);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result);
            Assert.Equal(date, result.First().Date);
            Assert.Equal(customerId, result.First().CustomerId);
        }

        private CreateReservationRequest DefaultReservationRequest() => CreateReservationRequest.Create(DateTime.Today, 1);
    }
}