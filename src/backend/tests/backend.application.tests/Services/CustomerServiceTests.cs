﻿using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Threading.Tasks;

using backend.application.Services;
using backend.domain;
using backend.domain.Requests;
using backend.infrastructure.Repositories.Interfaces;

using Microsoft.Extensions.Logging;

using Moq;

using Xunit;

namespace backend.application.tests.Services
{
    [ExcludeFromCodeCoverage]
    public class CustomerServiceTests
    {
        private readonly Mock<ILogger<CustomerService>> _loggerMock;
        private readonly Mock<IBookingDbRepository> _bookingDbRepoMock;

        public CustomerServiceTests()
        {
            _loggerMock = new Mock<ILogger<CustomerService>>();
            _bookingDbRepoMock = new Mock<IBookingDbRepository>();
        }

        [Fact]
        public async Task CreateCustomerAsyncTest()
        {
            // Arrange
            var customerId = 1;
            var request = CreateCustomerRequest.Create("name", "surname");

            _bookingDbRepoMock
                .Setup(mock => mock.CreateCustomerAsync(It.IsAny<CreateCustomerRequest>()))
                .ReturnsAsync(customerId);

            var sut = new CustomerService(_loggerMock.Object, _bookingDbRepoMock.Object);

            // Act
            var result = await sut.CreateCustomerAsync(request);

            // Assert
            Assert.Equal(customerId, result);
        }

        [Fact]
        public async Task GivenExceptionIsThrownByDBRepo_ThenCreateCustomerAsyncRethrows()
        {
            // Arrange
            var request = CreateCustomerRequest.Create("name", "surname");

            _bookingDbRepoMock
                .Setup(mock => mock.CreateCustomerAsync(It.IsAny<CreateCustomerRequest>()))
                .ThrowsAsync(new IOException());

            var sut = new CustomerService(_loggerMock.Object, _bookingDbRepoMock.Object);

            // Act & Assert
            var exception = await Assert.ThrowsAsync<IOException>(() => sut.CreateCustomerAsync(request));
        }

        [Fact]
        public async Task GetReservationsByCustomerIdTest()
        {
            // Arrange
            var domainCustomer = Customer.Create(id: 1, name: "name", surname: "surname");

            _bookingDbRepoMock
                .Setup(mock => mock.GetCustomerAsync(domainCustomer.Id))
                .ReturnsAsync(domainCustomer);

            var sut = new CustomerService(_loggerMock.Object, _bookingDbRepoMock.Object);

            // Act
            var result = await sut.GetCustomerAsync(domainCustomer.Id);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(domainCustomer.Id, result.Id);
            Assert.Equal(domainCustomer.Name, result.Name);
            Assert.Equal(domainCustomer.Surname, result.Surname);
        }
    }
}
