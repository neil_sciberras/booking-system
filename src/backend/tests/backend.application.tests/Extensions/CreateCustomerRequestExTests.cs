﻿using System.Diagnostics.CodeAnalysis;

using backend.api.contracts.Requests;
using backend.application.Extensions;

using Xunit;

namespace backend.application.tests.Extensions
{
    [ExcludeFromCodeCoverage]
    public class CreateCustomerRequestExTests
    {
        [Fact]
        public void ToDomainTest()
        {
            // Arrange
            var contractRequest = CreateCustomerRequest.Create("name", "surname");

            // Act
            var converted = contractRequest.ToDomain();

            // Assert
            Assert.Equal(contractRequest.Name, converted.Name);
            Assert.Equal(contractRequest.Surname, converted.Surname);
        }
    }
}
