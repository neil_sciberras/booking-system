﻿using System;
using System.Diagnostics.CodeAnalysis;

using backend.api.contracts.Requests;
using backend.application.Extensions;

using Xunit;

namespace backend.application.tests.Extensions
{
    [ExcludeFromCodeCoverage]
    public class CreateReservationRequestExTests
    {
        [Fact]
        public void ToDomainTest()
        {
            // Arrange
            var contractRequest = CreateReservationRequest.Create(DateTime.Today, 1);

            // Act
            var converted = contractRequest.ToDomain();

            // Assert
            Assert.Equal(contractRequest.Date, converted.Date);
            Assert.Equal(contractRequest.CustomerId, converted.CustomerId);
        }
    }
}
