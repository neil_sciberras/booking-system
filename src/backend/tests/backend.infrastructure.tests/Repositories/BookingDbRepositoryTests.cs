﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Threading.Tasks;

using backend.domain.Requests;
using backend.infrastructure.DI;
using backend.infrastructure.Repositories;

using Microsoft.Extensions.Logging;

using Moq;

using Xunit;

namespace backend.infrastructure.tests.Repositories
{
    [ExcludeFromCodeCoverage]
    public class BookingDbRepositoryTests
    {
        private readonly BookingDbRepository _sut;

        public BookingDbRepositoryTests()
        {
            var loggerMock = new Mock<ILogger<BookingDbRepository>>();
            var invalidConnectionStringOptions = new ConnectionStringOptions(string.Empty);

            _sut = new BookingDbRepository(loggerMock.Object, invalidConnectionStringOptions);
        }

        [Fact]
        public async Task GivenInvalidConnectionString_CreateReservationAsyncThrows()
        {
            // Arrange
            var request = CreateReservationRequest.Create(DateTime.Today, 1);

            // Act
            // Assert
            await Assert.ThrowsAsync<IOException>(() => _sut.CreateReservationAsync(request));
        }

        [Fact]
        public async Task GivenInvalidConnectionString_CreateCustomerAsyncThrows()
        {
            // Arrange
            var request = CreateCustomerRequest.Create("name", "surname");

            // Act
            // Assert
            await Assert.ThrowsAsync<IOException>(() => _sut.CreateCustomerAsync(request));
        }

        [Fact]
        public async Task GivenInvalidConnectionString_GetReservationsByCustomerIdThrows()
        {
            // Arrange
            // Act
            // Assert
            await Assert.ThrowsAsync<IOException>(() => _sut.GetReservationsByCustomerIdAsync(1));
        }

        [Fact]
        public async Task GivenInvalidConnectionString_GetCustomerAsyncThrows()
        {
            // Arrange
            // Act
            // Assert
            await Assert.ThrowsAsync<IOException>(() => _sut.GetCustomerAsync(1));
        }
    }
}
