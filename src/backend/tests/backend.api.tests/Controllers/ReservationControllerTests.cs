using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using backend.api.contracts;
using backend.api.Controllers;
using backend.application.Services.Interfaces;
using backend.domain.Requests;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Moq;

using Xunit;

namespace backend.api.tests.Controllers
{
    [ExcludeFromCodeCoverage]
    public class ReservationControllerTests
    {
        private readonly Mock<IReservationService> _reservationServiceMock;
        private readonly Mock<ILogger<ReservationController>> _loggerMock;

        public ReservationControllerTests()
        {
            _reservationServiceMock = new Mock<IReservationService>();
            _loggerMock = new Mock<ILogger<ReservationController>>();
        }

        [Fact]
        public async Task CreateReservationAsyncTest()
        {
            // Arrange
            var request = contracts.Requests.CreateReservationRequest.Create(DateTime.Today, 1);

            _reservationServiceMock.Setup(mock => mock.CreateReservationAsync(It.IsAny<CreateReservationRequest>()));

            var sut = new ReservationController(_loggerMock.Object, _reservationServiceMock.Object);

            // Act
            var result = (OkObjectResult)await sut.CreateReservationAsync(Guid.NewGuid(), request);

            // Assert
            Assert.Equal((int)HttpStatusCode.OK, result.StatusCode);
            Assert.NotNull(result.Value);
        }

        [Fact]
        public async Task GivenExceptionIsThrownByReservationService_ThenCreateReservationAsyncReturns500()
        {
            // Arrange
            var request = contracts.Requests.CreateReservationRequest.Create(DateTime.Today, 1);

            _reservationServiceMock
                .Setup(mock => mock.CreateReservationAsync(It.IsAny<CreateReservationRequest>()))
                .ThrowsAsync(new IOException());

            var sut = new ReservationController(_loggerMock.Object, _reservationServiceMock.Object);

            // Act
            var result = (StatusCodeResult)await sut.CreateReservationAsync(Guid.NewGuid(), request);

            // Assert
            Assert.Equal((int)HttpStatusCode.InternalServerError, result.StatusCode);
        }

        [Fact]
        public async Task GetReservationsByCustomerIdAsyncTest()
        {
            // Arrange
            var customerId = 1;
            var expectedReservation = Reservation.Create(date: DateTime.Today, customerId);

            _reservationServiceMock
                .Setup(mock => mock.GetReservationsByCustomerId(customerId))
                .ReturnsAsync(new List<Reservation>() { expectedReservation });

            var sut = new ReservationController(_loggerMock.Object, _reservationServiceMock.Object);

            // Act
            var result = (OkObjectResult)await sut.GetReservationsByCustomerIdAsync(Guid.NewGuid(), customerId);

            // Assert
            Assert.Equal((int)HttpStatusCode.OK, result.StatusCode);
            Assert.NotNull(result.Value);

            var actualReservation = ((IEnumerable<Reservation>)result.Value).First();

            Assert.Equal(expectedReservation.CustomerId, actualReservation.CustomerId);
            Assert.Equal(expectedReservation.Date, actualReservation.Date);
        }

        [Fact]
        public async Task GivenExceptionIsThrownByReservationService_ThenGetReservationsByCustomerIdAsyncReturns500()
        {
            // Arrange
            var customerId = 1;
            _reservationServiceMock
                .Setup(mock => mock.GetReservationsByCustomerId(customerId))
                .ThrowsAsync(new IOException());

            var sut = new ReservationController(_loggerMock.Object, _reservationServiceMock.Object);

            // Act
            var result = (StatusCodeResult)await sut.GetReservationsByCustomerIdAsync(Guid.NewGuid(), customerId);

            // Assert
            Assert.Equal((int)HttpStatusCode.InternalServerError, result.StatusCode);
        }
    }
}
