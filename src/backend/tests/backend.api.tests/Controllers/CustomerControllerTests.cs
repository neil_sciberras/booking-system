using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Net;
using System.Threading.Tasks;

using backend.api.contracts;
using backend.api.Controllers;
using backend.application.Services.Interfaces;
using backend.domain.Requests;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Moq;

using Xunit;

namespace backend.api.tests.Controllers
{
    [ExcludeFromCodeCoverage]
    public class CustomerControllerTests
    {
        private readonly Mock<ICustomerService> _customerServiceMock;
        private readonly Mock<ILogger<CustomerController>> _loggerMock;

        public CustomerControllerTests()
        {
            _customerServiceMock = new Mock<ICustomerService>();
            _loggerMock = new Mock<ILogger<CustomerController>>();
        }

        [Fact]
        public async Task CreateCustomerAsyncTest()
        {
            // Arrange
            var customerId = 1;
            var request = contracts.Requests.CreateCustomerRequest.Create("name", "surname");

            _customerServiceMock
                .Setup(mock => mock.CreateCustomerAsync(It.IsAny<CreateCustomerRequest>()))
                .ReturnsAsync(customerId);

            var sut = new CustomerController(_loggerMock.Object, _customerServiceMock.Object);

            // Act
            var result = (OkObjectResult)await sut.CreateCustomerAsync(Guid.NewGuid(), request);

            // Assert
            Assert.Equal((int)HttpStatusCode.OK, result.StatusCode.Value);
            Assert.Equal(customerId, (int)result.Value);
        }

        [Fact]
        public async Task GivenExceptionIsThrownByCustomerService_ThenCreateCustomerAsyncReturns500()
        {
            // Arrange
            var request = contracts.Requests.CreateCustomerRequest.Create("name", "surname");

            _customerServiceMock
                .Setup(mock => mock.CreateCustomerAsync(It.IsAny<CreateCustomerRequest>()))
                .ThrowsAsync(new IOException());

            var sut = new CustomerController(_loggerMock.Object, _customerServiceMock.Object);

            // Act
            var result = (StatusCodeResult)await sut.CreateCustomerAsync(Guid.NewGuid(), request);

            // Assert
            Assert.Equal((int)HttpStatusCode.InternalServerError, result.StatusCode);
        }

        [Fact]
        public async Task GetCustomerAsyncTest()
        {
            // Arrange
            var customerId = 1;
            var expectedCustomer = Customer.Create(customerId, name: "name", surname: "surname");

            _customerServiceMock
                .Setup(mock => mock.GetCustomerAsync(customerId))
                .ReturnsAsync(expectedCustomer);

            var sut = new CustomerController(_loggerMock.Object, _customerServiceMock.Object);

            // Act
            var result = (OkObjectResult)await sut.GetCustomerAsync(Guid.NewGuid(), customerId);

            // Assert
            Assert.Equal((int)HttpStatusCode.OK, result.StatusCode.Value);
            var returnedCustomer = ((Customer)result.Value);
            Assert.Equal(expectedCustomer.Id, returnedCustomer.Id);
            Assert.Equal(expectedCustomer.Name, returnedCustomer.Name);
            Assert.Equal(expectedCustomer.Surname, returnedCustomer.Surname);
        }

        [Fact]
        public async Task GivenExceptionIsThrownByCustomerService_ThenGetCustomerAsyncReturns500()
        {
            // Arrange
            var customerId = 1;
            _customerServiceMock
                .Setup(mock => mock.GetCustomerAsync(customerId))
                .ThrowsAsync(new IOException());

            var sut = new CustomerController(_loggerMock.Object, _customerServiceMock.Object);

            // Act
            var result = (StatusCodeResult)await sut.GetCustomerAsync(Guid.NewGuid(), customerId);

            // Assert
            Assert.Equal((int)HttpStatusCode.InternalServerError, result.StatusCode);
        }
    }
}
