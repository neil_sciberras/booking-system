﻿using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

using backend.api.contracts.Requests;
using backend.api.Controllers;
using backend.application.Services.Interfaces;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Moq;

using Xunit;

namespace backend.api.tests.Controllers
{
    [ExcludeFromCodeCoverage]
    public class ControllerTests
    {
        [Theory]
        [InlineData(false, true)]
        [InlineData(true, false)]
        [Description("Tests the ExecuteAndLog of the Controller class. This is exercised via the CustomerController, since the base Controller class's methods are protected.")]
        public async Task ExecuteAndLogTests(bool isCorrelationGuidPopulated, bool isRequestPopulated)
        {
            // Arrange
            var loggerMock = new Mock<ILogger<CustomerController>>();
            var serviceMock = new Mock<ICustomerService>();
            var sut = new CustomerController(loggerMock.Object, serviceMock.Object);

            var correlationGuid = (isCorrelationGuidPopulated) ? Guid.NewGuid() : Guid.Empty;
            var request = (isRequestPopulated) ? CreateCustomerRequest.Create("name", "surname") : null;

            // Act
            var result = await sut.CreateCustomerAsync(correlationGuid, request);

            // Assert
            if (!isCorrelationGuidPopulated)
            {
                var objectResult = (ObjectResult)result;
                Assert.Equal(StatusCodes.Status500InternalServerError, objectResult.StatusCode);
                Assert.Equal("correlationGuid header not provided", objectResult.Value.ToString());
            }
            else
            {
                var statusCodeResult = (StatusCodeResult)result;
                Assert.Equal(StatusCodes.Status500InternalServerError, statusCodeResult.StatusCode);
            }

        }
    }
}
