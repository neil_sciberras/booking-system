using System;
using System.Diagnostics.CodeAnalysis;

using backend.api.contracts.Requests;

using Newtonsoft.Json;

using Xunit;

namespace backend.api.contracts.tests.Requests
{
    [ExcludeFromCodeCoverage]
    public class CreateReservationRequestTests
    {
        [Fact]
        public void CorrectJsonDeserialization()
        {
            // Arrange
            var originalRequest = CreateReservationRequest.Create(DateTime.Today, 1);
            var serialized = JsonConvert.SerializeObject(originalRequest);

            // Act
            var deserialized = JsonConvert.DeserializeObject<CreateReservationRequest>(serialized);

            // Assert
            Assert.Equal(originalRequest.Date, deserialized.Date);
            Assert.Equal(originalRequest.CustomerId, deserialized.CustomerId);
        }
    }
}
