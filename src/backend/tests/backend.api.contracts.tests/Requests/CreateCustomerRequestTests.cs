using System.Diagnostics.CodeAnalysis;

using backend.api.contracts.Requests;

using Newtonsoft.Json;

using Xunit;

namespace backend.api.contracts.tests.Requests
{
    [ExcludeFromCodeCoverage]
    public class CreateCustomerRequestTests
    {
        [Fact]
        public void CorrectJsonDeserialization()
        {
            // Arrange
            var originalRequest = CreateCustomerRequest.Create("name", "surname");
            var serialized = JsonConvert.SerializeObject(originalRequest);

            // Act
            var deserialized = JsonConvert.DeserializeObject<CreateCustomerRequest>(serialized);

            // Assert
            Assert.Equal(originalRequest.Name, deserialized.Name);
            Assert.Equal(originalRequest.Surname, deserialized.Surname);
        }
    }
}
