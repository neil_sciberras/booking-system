﻿using System;
using Newtonsoft.Json;

namespace backend.api.contracts
{
    public class Reservation
    {
        public DateTime Date { get; set; }

        public int CustomerId { get; set; }

        [JsonConstructor]
        private Reservation(DateTime date, int customerId)
        {
            Date = date;
            CustomerId = customerId;
        }

        public static Reservation Create(DateTime date, int customerId)
        {
            return new Reservation(date, customerId);
        }
    }
}