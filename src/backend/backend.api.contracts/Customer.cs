﻿using Newtonsoft.Json;

namespace backend.api.contracts
{
    public class Customer
    {
        public int Id { get; }

        public string Name { get; set; }

        public string Surname { get; set; }

        [JsonConstructor]
        private Customer(int id, string name, string surname)
        {
            Id = id;
            Name = name;
            Surname = surname;
        }

        public static Customer Create(int id, string name, string surname)
        {
            return new Customer(id, name, surname);
        }
    }
}