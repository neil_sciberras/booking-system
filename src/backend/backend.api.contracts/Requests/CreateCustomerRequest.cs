﻿using System;

using Newtonsoft.Json;

namespace backend.api.contracts.Requests
{
    public class CreateCustomerRequest
    {
        public string Name { get; set; }
        
        public string Surname { get; set; }

        [JsonConstructor]
        private CreateCustomerRequest(string name, string surname)
        {
            Name = name;
            Surname = surname;
        }

        public static CreateCustomerRequest Create(string name, string surname)
        {
            return new CreateCustomerRequest(name, surname);
        }
    }
}
