﻿using System;

using Newtonsoft.Json;

namespace backend.api.contracts.Requests
{
    public class CreateReservationRequest
    {
        public DateTime Date { get; set; }

        public int CustomerId { get; set; }

        [JsonConstructor]
        private CreateReservationRequest(DateTime date, int customerId)
        {
            Date = date.ToUniversalTime();
            CustomerId = customerId;
        }

        public static CreateReservationRequest Create(DateTime date, int customerId)
        {
            return new CreateReservationRequest(date, customerId);
        }
    }
}