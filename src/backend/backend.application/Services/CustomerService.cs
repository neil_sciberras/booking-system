﻿using System.Threading.Tasks;

using backend.api.contracts;
using backend.application.Extensions;
using backend.application.Services.Interfaces;
using backend.domain.Requests;
using backend.infrastructure.Repositories.Interfaces;

using utilities;

using Microsoft.Extensions.Logging;

namespace backend.application.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ILogger<CustomerService> _logger;
        private readonly IBookingDbRepository _bookingDbRepository;

        public CustomerService(ILogger<CustomerService> logger, IBookingDbRepository bookingDbRepository)
        {
            _logger = logger.NotNull(nameof(logger));
            _bookingDbRepository = bookingDbRepository.NotNull(nameof(bookingDbRepository));
        }

        public async Task<int> CreateCustomerAsync(CreateCustomerRequest request)
        {
            _logger.LogInformation("{methodName}; Request: {@request}", nameof(CreateCustomerAsync), request);

            var customerId = await _bookingDbRepository.CreateCustomerAsync(request);

            _logger.LogInformation("{methodName}; CustomerId: {@customerId}", nameof(CreateCustomerAsync), customerId);

            return customerId;
        }

        public async Task<Customer> GetCustomerAsync(int customerId)
        {
            _logger.LogInformation("{methodName}; {@customerId}", nameof(GetCustomerAsync), new { CustomerId = customerId });

            var customer = await _bookingDbRepository.GetCustomerAsync(customerId);

            _logger.LogInformation("{methodName}; {@customer}", nameof(GetCustomerAsync), customer);

            return customer.ToContract();
        }
    }
}
