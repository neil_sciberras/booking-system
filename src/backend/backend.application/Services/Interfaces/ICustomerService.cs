﻿using System.Threading.Tasks;

using backend.api.contracts;
using backend.domain.Requests;

namespace backend.application.Services.Interfaces
{
    public interface ICustomerService
    {
        public Task<int> CreateCustomerAsync(CreateCustomerRequest request);

        public Task<Customer> GetCustomerAsync(int customerId);
    }
}
