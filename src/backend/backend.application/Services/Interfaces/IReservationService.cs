﻿using System.Collections.Generic;
using System.Threading.Tasks;

using backend.api.contracts;
using backend.domain.Requests;

namespace backend.application.Services.Interfaces
{
    public interface IReservationService
    {
        public Task<int> CreateReservationAsync(CreateReservationRequest request);

        public Task<IEnumerable<Reservation>> GetReservationsByCustomerId(int customerId);
    }
}
