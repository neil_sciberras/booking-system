﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using backend.api.contracts;
using backend.application.Extensions;
using backend.application.Services.Interfaces;
using backend.domain.Requests;
using backend.infrastructure.Repositories.Interfaces;

using utilities;

using Microsoft.Extensions.Logging;

namespace backend.application.Services
{
    public class ReservationService : IReservationService
    {
        private readonly ILogger<ReservationService> _logger;
        private readonly IBookingDbRepository _bookingDbRepository;

        public ReservationService(ILogger<ReservationService> logger, IBookingDbRepository bookingDbRepository)
        {
            _logger = logger.NotNull(nameof(logger));
            _bookingDbRepository = bookingDbRepository.NotNull(nameof(bookingDbRepository));
        }

        public async Task<int> CreateReservationAsync(CreateReservationRequest request)
        {
            _logger.LogInformation("{methodName}; Request: {@request}", nameof(CreateReservationAsync), request);

            var reservationId = await _bookingDbRepository.CreateReservationAsync(request);

            _logger.LogInformation("{methodName}; ReservationId: {@reservationId}", nameof(CreateReservationAsync), reservationId);

            return reservationId;
        }

        public async Task<IEnumerable<Reservation>> GetReservationsByCustomerId(int customerId)
        {
            _logger.LogInformation("{methodName}; {@customerId}", nameof(GetReservationsByCustomerId), new { CustomerId = customerId });

            var reservations = await _bookingDbRepository.GetReservationsByCustomerIdAsync(customerId);

            _logger.LogInformation("{methodName}; {@reservations}", nameof(GetReservationsByCustomerId), reservations);

            return reservations.Select(reservation => reservation.ToContract());
        }
    }
}
