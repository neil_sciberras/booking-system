﻿using System.Diagnostics.CodeAnalysis;

using backend.application.Services;
using backend.application.Services.Interfaces;

using Microsoft.Extensions.DependencyInjection;

namespace backend.application
{
    [ExcludeFromCodeCoverage]
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddApplicationDependencies(this IServiceCollection services)
        {
            services.AddSingleton<ICustomerService, CustomerService>();
            services.AddSingleton<IReservationService, ReservationService>();

            return services;
        }
    }
}
