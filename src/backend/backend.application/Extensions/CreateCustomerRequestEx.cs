﻿using backend.domain.Requests;

namespace backend.application.Extensions
{
    public static class CreateCustomerRequestEx
    {
        public static CreateCustomerRequest ToDomain(this api.contracts.Requests.CreateCustomerRequest request)
        {
            return CreateCustomerRequest.Create(request.Name, request.Surname);
        }
    }
}
