﻿using backend.domain.Requests;

namespace backend.application.Extensions
{
    public static class CreateReservationRequestEx
    {
        public static CreateReservationRequest ToDomain(this api.contracts.Requests.CreateReservationRequest request)
        {
            return CreateReservationRequest.Create(request.Date, request.CustomerId);
        }
    }
}
