﻿using backend.api.contracts;

namespace backend.application.Extensions
{
    public static class ReservationEx
    {
        public static Reservation ToContract(this domain.Reservation reservation)
        {
            return Reservation.Create(reservation.Date, reservation.CustomerId);
        }
    }
}
