﻿using backend.api.contracts;

namespace backend.application.Extensions
{
    public static class CustomerEx
    {
        public static Customer ToContract(this domain.Customer customer)
        {
            return Customer.Create(customer.Id, customer.Name, customer.Surname);
        }
    }
}
