﻿using System;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using utilities;

namespace backend.api.Controllers
{
    public class Controller<T>
    {
        private readonly ILogger<T> _logger;

        protected Controller(ILogger<T> logger)
        {
            _logger = logger.NotNull(nameof(logger));
        }

        protected async Task<ActionResult> ExecuteAndLog(string methodName, Guid correlationGuid, object request, Func<Task<ActionResult>> action)
        {
            try
            {
                if (correlationGuid == Guid.Empty)
                    throw new ArgumentException(nameof(correlationGuid));

                if (request == null)
                    throw new ArgumentNullException(nameof(request));

                if (action == null)
                    throw new ArgumentNullException(nameof(request));

                using (_logger.BeginScope("{@scope}", new { MethodName = methodName, CorrelationGuid = correlationGuid }))
                {
                    _logger.LogInformation("{@request}", request);

                    var response = await action();

                    _logger.LogInformation("{@response}", response);

                    return response;
                }
            }
            catch (ArgumentNullException e)
            {
                _logger.LogError(e.Message + "; Exception: {@exception}", e);

                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
            catch (ArgumentException e)
            {
                _logger.LogError(e.Message + "; Exception: {@exception}", e);

                return new ObjectResult("correlationGuid header not provided") { StatusCode = StatusCodes.Status500InternalServerError };
            }
            catch (Exception e)
            {
                _logger.LogError("{methodName} Error; {@request}; Exception: {@exception}", methodName, request, e);
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
