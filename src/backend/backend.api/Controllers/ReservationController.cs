﻿using System;
using System.Threading.Tasks;

using backend.api.contracts.Requests;
using backend.application.Extensions;
using backend.application.Services.Interfaces;
using utilities;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace backend.api.Controllers
{
    [ApiController]
    public class ReservationController : Controller<ReservationController>
    {
        private readonly IReservationService _reservationService;

        public ReservationController(ILogger<ReservationController> logger, IReservationService reservationService) : base(logger)
        {
            _reservationService = reservationService.NotNull(nameof(reservationService));
        }

        /// <summary>
        /// Creates a reservation in BookingDB
        /// </summary>
        /// <param name="correlationGuid"></param>
        /// <param name="request">Holds the reservation information</param>
        /// <returns>200 When reservation is added successfully</returns>
        /// <returns>500 When writing to DB was unsuccessful</returns>
        [HttpPost]
        [Route("reservation/create")]
        public async Task<IActionResult> CreateReservationAsync([FromHeader] Guid correlationGuid, [FromBody] CreateReservationRequest request)
        {
            return await ExecuteAndLog(
                methodName: nameof(CreateReservationAsync),
                correlationGuid: correlationGuid,
                request: request,
                action: async () => new OkObjectResult(await _reservationService.CreateReservationAsync(request.ToDomain())));
        }

        /// <summary>
        /// Gets a customer's reservations from the BookingDB
        /// </summary>
        /// <param name="correlationGuid"></param>
        /// <param name="customerId">The customerId</param>
        /// <returns>200 When reservations are retrieved successfully</returns>
        /// <returns>500 When retrieving from DB is unsuccessful</returns>
        [HttpGet]
        [Route("reservations/of/customer/{customerId}")]
        public async Task<IActionResult> GetReservationsByCustomerIdAsync([FromHeader] Guid correlationGuid, [FromRoute] int customerId)
        {
            return await ExecuteAndLog(
                methodName: nameof(GetReservationsByCustomerIdAsync),
                correlationGuid: correlationGuid,
                request: customerId,
                action: async () => new OkObjectResult(await _reservationService.GetReservationsByCustomerId(customerId)));
        }
    }
}