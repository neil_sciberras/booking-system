﻿using System;
using System.Threading.Tasks;

using backend.api.contracts.Requests;
using backend.application.Extensions;
using backend.application.Services.Interfaces;
using utilities;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace backend.api.Controllers
{
    [ApiController]
    [Route("customer")]
    public class CustomerController : Controller<CustomerController>
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ILogger<CustomerController> logger, ICustomerService customerService) : base(logger)
        {
            _customerService = customerService.NotNull(nameof(customerService));
        }

        /// <summary>
        /// Creates a customer in BookingDB
        /// </summary>
        /// <param name="correlationGuid"></param>
        /// <param name="request">Holds the customer information</param>
        /// <returns>200 When customer is added successfully</returns>
        /// <returns>500 When writing to DB is unsuccessful</returns>
        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> CreateCustomerAsync([FromHeader] Guid correlationGuid, [FromBody] CreateCustomerRequest request)
        {
            return await ExecuteAndLog(
                methodName: nameof(CreateCustomerAsync),
                correlationGuid: correlationGuid,
                request: request,
                action: async () => new OkObjectResult(await _customerService.CreateCustomerAsync(request.ToDomain())));
        }

        /// <summary>
        /// Gets a customer from the BookingDB
        /// </summary>
        /// <param name="correlationGuid"></param>
        /// <param name="customerId">The customerId</param>
        /// <returns>200 When customer is retrieved successfully</returns>
        /// <returns>500 When retrieving from DB is unsuccessful</returns>
        [HttpGet]
        [Route("{customerId}")]
        public async Task<IActionResult> GetCustomerAsync([FromHeader] Guid correlationGuid, [FromRoute] int customerId)
        {
            return await ExecuteAndLog(
                methodName: nameof(GetCustomerAsync),
                correlationGuid: correlationGuid,
                request: customerId,
                action: async () => new OkObjectResult(await _customerService.GetCustomerAsync(customerId)));
        }
    }
}
