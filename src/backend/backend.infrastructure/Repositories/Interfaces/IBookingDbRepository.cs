﻿using System.Collections.Generic;
using System.Threading.Tasks;

using backend.domain;
using backend.domain.Requests;

namespace backend.infrastructure.Repositories.Interfaces
{
    public interface IBookingDbRepository
    {
        public Task<int> CreateCustomerAsync(CreateCustomerRequest createCustomerRequest);
        
        public Task<Customer> GetCustomerAsync(int customerId);

        public Task<int> CreateReservationAsync(CreateReservationRequest createReservationRequest);

        public Task<IEnumerable<Reservation>> GetReservationsByCustomerIdAsync(int customerId);
    }
}
