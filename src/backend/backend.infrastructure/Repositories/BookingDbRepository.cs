﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

using backend.domain;
using backend.domain.Requests;
using backend.infrastructure.DI;
using backend.infrastructure.Repositories.Interfaces;

using utilities;

using Microsoft.Extensions.Logging;

namespace backend.infrastructure.Repositories
{
    public class BookingDbRepository : IBookingDbRepository
    {
        private const string SP_CUSTOMER_CREATE = "dbo.spCustomer_Create";
        private const string SP_CUSTOMER_GET = "dbo.spCustomer_Get";
        private const string SP_RESERVATION_CREATE = "dbo.spReservation_Create";
        private const string SP_RESERVATION_GET_BYCUSTOMERID = "dbo.spReservation_GetByCustomerId";

        private readonly ILogger<BookingDbRepository> _logger;
        private readonly ConnectionStringOptions _connectionStringOptions;

        public BookingDbRepository(ILogger<BookingDbRepository> logger, ConnectionStringOptions connectionStringOptions)
        {
            _logger = logger.NotNull(nameof(logger));
            _connectionStringOptions = connectionStringOptions.NotNull(nameof(connectionStringOptions));
        }
        
        public async Task<int> CreateReservationAsync(CreateReservationRequest createReservationRequest)
        {
            try
            {
                await using var connection = await GetConnection();

                var command = new SqlCommand(SP_RESERVATION_CREATE, connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.Add(new SqlParameter("@Date", createReservationRequest.Date));
                command.Parameters.Add(new SqlParameter("@CustomerId", createReservationRequest.CustomerId));

                var returnParam = command.Parameters.Add("RetVal", SqlDbType.Int);
                returnParam.Direction = ParameterDirection.ReturnValue;

                await command.ExecuteNonQueryAsync();

                return (int)returnParam.Value;
            }
            catch (Exception e)
            {
                _logger.LogError("{methodName} Error; {@request}; Exception: {@exception}",
                    nameof(CreateReservationAsync), createReservationRequest, e);
                throw new IOException(e.Message);
            }
        }

        public async Task<Customer> GetCustomerAsync(int customerId)
        {
            try
            {
                await using var connection = await GetConnection();

                var command = new SqlCommand(SP_CUSTOMER_GET, connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.Add(new SqlParameter("@Id", customerId));

                var reader = await command.ExecuteReaderAsync();

                reader.Read();

                return Customer.Create(
                    (int)reader["Id"],
                    (string)reader["Name"],
                    (string)reader["Surname"]);
            }
            catch (Exception e)
            {
                _logger.LogError("{methodName} Error; {@customerId}; Exception: {@exception}",
                    nameof(GetCustomerAsync), new { CustomerId = customerId }, e);
                throw new IOException(e.Message);
            }
        }

        public async Task<int> CreateCustomerAsync(CreateCustomerRequest createCustomerRequest)
        {
            try
            {
                await using var connection = await GetConnection();

                var command = new SqlCommand(SP_CUSTOMER_CREATE, connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.Add(new SqlParameter("@Name", createCustomerRequest.Name));
                command.Parameters.Add(new SqlParameter("@Surname", createCustomerRequest.Surname));

                var returnParam = command.Parameters.Add("RetVal", SqlDbType.Int);
                returnParam.Direction = ParameterDirection.ReturnValue;

                await command.ExecuteNonQueryAsync();

                return (int)returnParam.Value;
            }
            catch (Exception e)
            {
                _logger.LogError("{methodName} Error; {@request}; Exception: {@exception}",
                    nameof(CreateCustomerAsync), createCustomerRequest, e);
                throw new IOException(e.Message);
            }
        }

        public async Task<IEnumerable<Reservation>> GetReservationsByCustomerIdAsync(int customerId)
        {
            try
            {
                await using var connection = await GetConnection();

                var command = new SqlCommand(SP_RESERVATION_GET_BYCUSTOMERID, connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.Add(new SqlParameter("@CustomerId", customerId));

                var reader = await command.ExecuteReaderAsync();
                var listOfReservations = new List<Reservation>();

                while (reader.Read())
                {
                    listOfReservations.Add(Reservation.Create(
                        (DateTime)reader["Date"],
                        (int)reader["CustomerId"]));
                }

                return listOfReservations;
            }
            catch (Exception e)
            {
                _logger.LogError("{methodName} Error; {@customerId}; Exception: {@exception}",
                    nameof(GetReservationsByCustomerIdAsync), new { CustomerId = customerId }, e);
                throw new IOException(e.Message);
            }
        }

        private async Task<SqlConnection> GetConnection()
        {
            var connection = new SqlConnection(_connectionStringOptions.BookingDB);
            await connection.OpenAsync();
            return connection;
        }
    }
}
