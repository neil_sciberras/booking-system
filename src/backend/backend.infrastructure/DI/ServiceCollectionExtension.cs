﻿using System.Diagnostics.CodeAnalysis;

using backend.infrastructure.Repositories;
using backend.infrastructure.Repositories.Interfaces;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace backend.infrastructure.DI
{
    [ExcludeFromCodeCoverage]
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddInfrastructureDependencies(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(_ => new ConnectionStringOptions(configuration.GetConnectionString("BookingDB")));
            services.AddSingleton<IBookingDbRepository, BookingDbRepository>();

            return services;
        }
    }
}