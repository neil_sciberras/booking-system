﻿using System.Diagnostics.CodeAnalysis;

namespace backend.infrastructure.DI
{
    [ExcludeFromCodeCoverage]
    public class ConnectionStringOptions
    {
        public string BookingDB { get; set; }

        public ConnectionStringOptions(string bookingDb)
        {
            BookingDB = bookingDb;
        }
    }
}
