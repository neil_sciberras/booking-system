using System;

using utilities;

using Xunit;

namespace utilties.tests
{
    public class ArgCheckTests
    {
        [Fact]
        public void GivenNullArg_ThenExceptionIsThrown()
        {
            // Arrange
            // Act
            var obj = (int?)null;
            var e = Assert.Throws<ArgumentNullException>(() => obj.NotNull(nameof(obj)));

            // Assert
            Assert.Equal("Value cannot be null. (Parameter 'obj')", e.Message);
        }

        [Fact]
        public void GivenNotNullArg_ThenValueIsReturned()
        {
            // Arrange
            // Act
            var obj = 1;
            var returnedObj = obj.NotNull(nameof(obj));

            // Assert
            Assert.Equal(obj, returnedObj);
        }
    }
}