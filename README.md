This is a personal side project in which I experiment and learn along the way.

The idea behing it is a booking-system enabling the end user to book reservations in salons, hairdressers, etc. direct from his mobile phone.

The current state of the project contains an MVP version of the backend, written in C# and consisting of a Rest API, database solution and automated feature tests.

The near-future goal is to create a mobile app, which integrates with the backend API to create reservations.
