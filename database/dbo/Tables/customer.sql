﻿CREATE TABLE [dbo].[customer]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(50) NULL, 
    [Surname] NVARCHAR(50) NULL
)
