﻿CREATE PROCEDURE [dbo].[spReservation_Create]
	@Date datetime,
	@CustomerId int
AS
	INSERT INTO dbo.reservation (Date, CustomerId)
	VALUES (@Date, @CustomerId)

	RETURN SCOPE_IDENTITY()