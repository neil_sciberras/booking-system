﻿CREATE PROCEDURE [dbo].[spReservation_GetByCustomerId]
	@CustomerId int
AS
	SELECT * FROM dbo.reservation WHERE CustomerId = @CustomerId
GO