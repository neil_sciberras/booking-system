﻿CREATE PROCEDURE [dbo].[spCustomer_Create]
	@Name nvarchar(50),
	@Surname nvarchar(50)
AS
	INSERT INTO dbo.customer (Name, Surname)
	VALUES (@Name, @Surname)
	
	RETURN SCOPE_IDENTITY()
