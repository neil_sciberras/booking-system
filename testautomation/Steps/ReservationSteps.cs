﻿using System;
using System.Linq;
using System.Threading.Tasks;

using FluentAssertions;

using TechTalk.SpecFlow;

using testautomation.Drivers;
using testautomation.Utilities;

namespace testautomation.Steps
{
    [Binding]
    public class ReservationSteps : Steps
    {
        private readonly ReservationApiDriver _reservationApiDriver;
        public ReservationSteps(ScenarioContext scenarioContext, ReservationApiDriver reservationApiDriver) : base(scenarioContext)
        {
            _reservationApiDriver = reservationApiDriver;
        }

        [When(@"a reservation is created for today")]
        public async Task WhenAReservationIsCreatedForToday()
        {
            var customerId = ScenarioContext.Get<int>(ContextKeys.CustomerId);

            var reservationDate = DateTime.Now.ToUniversalTime();
            var reservationId = await _reservationApiDriver.CreateReservation(reservationDate, customerId);

            ScenarioContext.Set(reservationId, ContextKeys.ReservationId);
            ScenarioContext.Set(reservationDate, ContextKeys.ReservationDate);
        }

        [Then(@"the GetReservation API returns the correct data")]
        public async Task ThenTheGetReservationAPIReturnsTheCorrectData()
        {
            var customerId = ScenarioContext.Get<int>(ContextKeys.CustomerId);
            var expectedReservationDate = ScenarioContext.Get<DateTime>(ContextKeys.ReservationDate);

            var reservationList = (await _reservationApiDriver.GetReservationsOfCustomer(customerId)).ToList();

            reservationList.Should().NotBeNull();
            reservationList.Should().NotBeEmpty();
            reservationList.First().CustomerId.Should().Be(customerId);
            reservationList.First().Date.TrimMilliseconds().Should().Be(expectedReservationDate.TrimMilliseconds());
        }
    }
}
