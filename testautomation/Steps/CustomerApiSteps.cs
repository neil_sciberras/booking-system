﻿using System.Threading.Tasks;

using FluentAssertions;

using TechTalk.SpecFlow;

using testautomation.Drivers;

namespace testautomation.Steps
{
    [Binding]
    public class CustomerApiSteps : Steps
    {
        private readonly CustomerApiDriver _customerApiDriver;

        public CustomerApiSteps(ScenarioContext scenarioContext, CustomerApiDriver customerApiDriver) : base(scenarioContext)
        {
            _customerApiDriver = customerApiDriver;
        }

        [Given(@"a customer (.*) (.*) is created")]
        public async Task GivenACustomerIsCreated(string name, string surname)
        {
            var customerId = await _customerApiDriver.CreateCustomer(name, surname);

            ScenarioContext.Set(name, ContextKeys.CustomerName);
            ScenarioContext.Set(surname, ContextKeys.CustomerSurname);
            ScenarioContext.Set(customerId, ContextKeys.CustomerId);
        }

        [Then(@"the GetCustomer API returns the correct data")]
        public async Task ThenTheGetCustomerAPIReturnsTheCorrectData()
        {
            var customerId = ScenarioContext.Get<int>(ContextKeys.CustomerId);

            var customer = await _customerApiDriver.GetCustomer(customerId);

            customer.Id.Should().Be(customerId);
            customer.Name.Should().Be(ScenarioContext.Get<string>(ContextKeys.CustomerName));
            customer.Surname.Should().Be(ScenarioContext.Get<string>(ContextKeys.CustomerSurname));
        }

    }
}
