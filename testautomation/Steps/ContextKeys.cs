﻿namespace testautomation.Steps
{
    public static class ContextKeys
    {
        public static readonly string CustomerName = "CustomerName";
        public static readonly string CustomerSurname = "CustomerSurname";
        public static readonly string CustomerId = "CustomerId";

        public static readonly string ReservationId = "ReservationId";
        public static readonly string ReservationDate = "ReservationDate";
    }
}
