﻿namespace testautomation.Utilities
{
    public interface IConfig
    {
        string BaseUrl { get; }
        string CreateCustomerPath { get; }
        string GetCustomerPath(int customerId);
        string CreateReservationPath { get; }
        string GetReservationOfCustomerPath(int customerId);
    }
}
