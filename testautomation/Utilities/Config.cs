﻿using Microsoft.Extensions.Configuration;

namespace testautomation.Utilities
{
    public class Config : IConfig
    {
        private readonly IConfiguration _configuration;

        public Config(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string BaseUrl => _configuration["BaseUrl"];
        public string CreateCustomerPath => _configuration["Paths:Customer:Create"];
        public string GetCustomerPath(int customerId) => string.Format(_configuration["Paths:Customer:Get"], customerId);
        public string CreateReservationPath => _configuration["Paths:Reservation:Create"];
        public string GetReservationOfCustomerPath(int customerId) => string.Format(_configuration["Paths:Reservation:OfCustomer"], customerId);
    }
}