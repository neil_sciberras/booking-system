﻿using System;
using System.Collections.Generic;
using System.Text;

namespace testautomation.Utilities
{
    public static class DateTimeEx
    {
        public static DateTime TrimMilliseconds(this DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second, 0, dt.Kind);
        }
    }
}
