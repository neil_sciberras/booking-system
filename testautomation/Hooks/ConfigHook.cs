﻿using System.IO;
using BoDi;
using Microsoft.Extensions.Configuration;
using TechTalk.SpecFlow;
using TechTalk.SpecRun;
using testautomation.Utilities;

namespace testautomation.Hooks
{
    [Binding]
    public class ConfigHook
    {
        [BeforeTestRun]
        public static void InitializeConfig(IObjectContainer objectContainer, TestRunContext testRunContext)
        {
            var configurationRoot = new ConfigurationBuilder()
                .AddJsonFile(Path.Combine(testRunContext.TestDirectory, "appsettings.json"))
                .Build();

            var config = new Config(configurationRoot);

            objectContainer.RegisterInstanceAs<IConfig>(config);
        }
    }
}
