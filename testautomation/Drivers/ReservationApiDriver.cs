﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using backend.api.contracts;
using backend.api.contracts.Requests;

using testautomation.Utilities;

namespace testautomation.Drivers
{
    public class ReservationApiDriver : ApiDriver
    {
        public ReservationApiDriver(IConfig config) : base(config)
        {
        }

        public async Task<int> CreateReservation(DateTime date, int customerId)
        {
            return await ExecutePost<CreateReservationRequest, int>(
                path: Config.CreateReservationPath,
                request: CreateReservationRequest.Create(date, customerId));
        }

        public async Task<IEnumerable<Reservation>> GetReservationsOfCustomer(int customerId)
        {
            return await ExecuteGet<IEnumerable<Reservation>>(path: Config.GetReservationOfCustomerPath(customerId));
        }
    }
}
