﻿using System.Threading.Tasks;

using testautomation.Utilities;

using backend.api.contracts;
using backend.api.contracts.Requests;

namespace testautomation.Drivers
{
    public class CustomerApiDriver : ApiDriver
    {
        public CustomerApiDriver(IConfig config) : base(config)
        {
        }

        public async Task<int> CreateCustomer(string name, string surname)
        {
            return await ExecutePost<CreateCustomerRequest, int>(
                path: Config.CreateCustomerPath, 
                request: CreateCustomerRequest.Create(name, surname));
        }

        public async Task<Customer> GetCustomer(int customerId)
        {
            return await ExecuteGet<Customer>(path: Config.GetCustomerPath(customerId));
        }
    }
}
