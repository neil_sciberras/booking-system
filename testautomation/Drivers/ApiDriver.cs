﻿using System;
using System.Net;
using System.Threading.Tasks;

using FluentAssertions;

using Newtonsoft.Json;

using RestSharp;

using testautomation.Utilities;

namespace testautomation.Drivers
{
    public abstract class ApiDriver
    {
        protected readonly IConfig Config;
        protected readonly RestClient RestClient;

        protected ApiDriver(IConfig config)
        {
            Config = config;
            RestClient = new RestClient(Config.BaseUrl);
        }

        protected async Task<TOut> ExecutePost<TIn, TOut>(string path, TIn request)
        {
            var restRequest = new RestRequest(path, Method.POST);
            restRequest = AddCorrelationGuid(restRequest);
            restRequest.AddJsonBody(request);

            return await ExecuteRequest<TOut>(restRequest);
        }

        protected async Task<TOut> ExecuteGet<TOut>(string path)
        {
            var request = new RestRequest(path, Method.GET);
            request = AddCorrelationGuid(request);

            return await ExecuteRequest<TOut>(request);
        }

        private async Task<TOut> ExecuteRequest<TOut>(RestRequest request)
        {
            var response = await RestClient.ExecuteAsync(request);

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.Should().NotBeNull();

            return JsonConvert.DeserializeObject<TOut>(response.Content);
        }

        private RestRequest AddCorrelationGuid(RestRequest request)
        {
            return (RestRequest)request.AddHeader("correlationGuid", Guid.NewGuid().ToString());
        }
    }
}
