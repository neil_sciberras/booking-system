﻿$packageName = $args[0]
$version = $args[1]

if($packageName -eq "backend.api.contracts")
{
    $packageLocation = "src\backend\backend.api.contracts\bin\Debug\backend.api.contracts." + $version + ".nupkg"
}

if($packageName -eq "utilities")
{
    $packageLocation = "src\utilities\utilities\bin\Debug\utilities." + $version + ".nupkg"
}

nuget add $packageLocation -Source local-nuget-feed